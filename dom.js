
$(document).ready(function(){ 
	var isFolded = false;
	var counter = 0;
	var prevWindowSize = $(window).width();
	var currArticle = 'om';

	initMenu();
	$('#maincontent').load('articles/om.html');
	
	if($(window).width() < 400){
		$('#logo').attr('src', "resources/mobileLogo.png");
	}

	$(".navItem").click(function () {
		var id = this.id;
		var scrollDestination;

		$('.navItem').css("fontStyle", 'normal');
		$(this).css("fontStyle", 'italic');

		
		
		
		//scroll
		if(id === "logo" && $(document).scrollTop() > 0){
			scrollDestination = 0;
		
		}else if(id === 'kontakt'){
			scrollDestination = $("#contact").offset().top - 80;
		}else{
			scrollDestination = $("#maincontent").offset().top- 150;
		}

		$('#menubar').animate({
			height: 0
		}, 400);

		if($('a').attr('class') === 'navItem minMenuItem'){
			counter++;
		}

		 $('a').removeClass('minMenuItem');

		if(($(document).scrollTop() != scrollDestination)){
			
			$('html, body').animate({
					scrollTop: scrollDestination
				}, 1500);
		}

		if(currArticle !== id){
			loadArticle(id);
		}

	});

	function loadArticle (id) {	
		if(id !== 'kontakt' && id !== 'logo'){
			//$('#maincontent').empty();
			$('#maincontent').hide().load('articles/'+ id + '.html').fadeIn(1000);
			currArticle = id;
		}
	}

	$("#menuicon").click(function() {

		if(counter % 2 == 0){
			

			$('#menubar').css({
				'width': '100%'
			});

			

			$('#menubar').animate({
				backgroundColor: "white",
				height: 280
				
			}, 400);


			$('a').addClass('minMenuItem');
			
		}else{
			
			$('#menubar').animate({
				height: 0
			}, 400);

			$('a').removeClass('minMenuItem');
		
		}
		
		counter++;

	});

});